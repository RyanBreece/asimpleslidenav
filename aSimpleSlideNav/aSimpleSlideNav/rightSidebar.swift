//
//  Sidebar.swift
//  Sidebar
//
//  Created by Ryan Breece on 4/5/16.
//  Copyright © 2016 Ryan Breece. All rights reserved.
//

import UIKit

@objc protocol rightSideBarDelegate{
    func rightsidebarDidSelectButtonAtIndex(index: Int)
    optional func rightSidebarWillClose()
    optional func rightSidebarWillOpen()
    
}


class rightSidebar: NSObject, rightsideBarTableViewControllerDelegate {
    
    let barWidth: CGFloat = 350.0
    let sidebarTableViewTopInset: CGFloat = 150.0
    let sidebarContainerView: UIView = UIView()
    let sideBarTableViewController: SidebarTableViewController = SidebarTableViewController()
    
    var originView: UIView!
    var animator: UIDynamicAnimator!
    var delegate: rightSideBarDelegate?
    var isSidebarOpen: Bool = false
    
    override init() {
        super.init()
    }
    
    init(sourceView: UIView, menuItems: [String]){
        super.init()
        
        originView = sourceView
        sideBarTableViewController.tableData = menuItems
        
        setupSidebar()
        
        animator = UIDynamicAnimator(referenceView: originView)
        let showGestureReconizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        showGestureReconizer.direction = UISwipeGestureRecognizerDirection.Right
        originView.addGestureRecognizer(showGestureReconizer)
        
        let hideGestureReconizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        hideGestureReconizer.direction = UISwipeGestureRecognizerDirection.Left
        originView.addGestureRecognizer(hideGestureReconizer)
        
    }
    
    func setupSidebar() {
        
        sidebarContainerView.frame = CGRectMake(-barWidth - 1, originView.frame.origin.y, barWidth, originView.frame.size.height)
        sidebarContainerView.backgroundColor = UIColor.clearColor()
        sidebarContainerView.clipsToBounds = false
        originView.addSubview(sidebarContainerView)
        
        let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        blurView.frame = sidebarContainerView.bounds
        sidebarContainerView.addSubview(blurView)
        
        sideBarTableViewController.rightdelegate = self
        sideBarTableViewController.tableView.frame = sidebarContainerView.bounds
        sideBarTableViewController.tableView.clipsToBounds = false
        sideBarTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        sideBarTableViewController.tableView.backgroundColor = UIColor.clearColor()
        sideBarTableViewController.tableView.scrollsToTop = false
        sideBarTableViewController.tableView.contentInset = UIEdgeInsetsMake(sidebarTableViewTopInset, 0, 0, 0)
        
        sideBarTableViewController.tableView.reloadData()
        sidebarContainerView.addSubview(sideBarTableViewController.tableView)
        
        
    }
    
    func handleSwipe(recognizer: UISwipeGestureRecognizer) {
        if recognizer.direction == UISwipeGestureRecognizerDirection.Left {
            showSidebar(false)
            delegate?.rightSidebarWillClose?()
            
        } else {
            showSidebar(true)
            delegate?.rightSidebarWillOpen?()
        }
        
        
    }
    func showSidebar(shouldOpen: Bool) {
        
        animator.removeAllBehaviors()
        isSidebarOpen = shouldOpen
        
        let gravityX: CGFloat = (shouldOpen) ? 10.0 : -10.0
        let magnitude: CGFloat = (shouldOpen) ? 10 : -10
        let boundryX: CGFloat = (shouldOpen ? barWidth : -barWidth - 1)
        
        let gravityBehavior: UIGravityBehavior = UIGravityBehavior(items: [sidebarContainerView])
        gravityBehavior.gravityDirection = CGVectorMake(gravityX, 0)
        animator.addBehavior(gravityBehavior)
        
        let collisionBehavior: UICollisionBehavior = UICollisionBehavior(items: [sidebarContainerView])
        collisionBehavior.addBoundaryWithIdentifier("sideBoundry", fromPoint: CGPointMake(boundryX, 20), toPoint: CGPointMake(boundryX, originView.frame.size.height))
        animator.addBehavior(collisionBehavior)
        
        let pushBehavior: UIPushBehavior = UIPushBehavior(items: [sidebarContainerView], mode: UIPushBehaviorMode.Instantaneous)
        pushBehavior.magnitude = magnitude
        animator.addBehavior(pushBehavior)
        
        let sidebarBehavior: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [sidebarContainerView])
        sidebarBehavior.elasticity = 0.3
        animator.addBehavior(sidebarBehavior)
        
        
    }
    
    func rightsidebarControlDidSelectRow(indexpath: NSIndexPath) {
        
        delegate?.rightsidebarDidSelectButtonAtIndex(indexpath.row)
    }
    
    
}





























