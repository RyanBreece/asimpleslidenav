//
//  SidebarTableViewController.swift
//  Sidebar
//
//  Created by Ryan Breece on 4/5/16.
//  Copyright © 2016 Ryan Breece. All rights reserved.
//

import UIKit

protocol leftsideBarTableViewControllerDelegate {
    func leftsidebarControlDidSelectRow(indexPath: NSIndexPath)
    
}

protocol rightsideBarTableViewControllerDelegate {
    func rightsidebarControlDidSelectRow(indexPath: NSIndexPath)
    
}


class SidebarTableViewController: UITableViewController {

    var leftdelegate: leftsideBarTableViewControllerDelegate?
    var rightdelegate: rightsideBarTableViewControllerDelegate?
    var tableData = [String]()
    


    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell?
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            
            // Configure the cell...
            
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.darkTextColor()
            
            let selectedView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: cell!.frame.size.width, height: cell!.frame.size.height))
            
            selectedView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
            
            cell!.selectedBackgroundView = selectedView
            
        }
        
        cell!.textLabel?.text = tableData[indexPath.row]
        

        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45.0
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        leftdelegate?.leftsidebarControlDidSelectRow(indexPath)
        rightdelegate?.rightsidebarControlDidSelectRow(indexPath)
        
    }

}
