//
//  ViewController.swift
//  Sidebar
//
//  Created by Ryan Breece on 4/5/16.
//  Copyright © 2016 Ryan Breece. All rights reserved.
//

import UIKit


class ViewController: UIViewController, leftSideBarDelegate, rightSideBarDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var leftsidebar: leftSidebar = leftSidebar()
    var rightsidebar: rightSidebar = rightSidebar()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // left menu
        
        leftsidebar = leftSidebar(sourceView: self.view, menuItems: ["first", "second", "third", "fourth", "fifth"])
        leftsidebar.delegate = self
        
        //right menu
        
        rightsidebar = rightSidebar(sourceView: self.view, menuItems: ["1", "2", "3", "4", "5"])
        rightsidebar.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func leftMenuPress(sender: UIButton) {
        
        if leftsidebar.isSidebarOpen == false {
        leftsidebar.showSidebar(true)
        leftsidebar.delegate?.leftSidebarWillOpen?()
        } else {
            
        }
        
    }
    @IBAction func rightMenuPress(sender: AnyObject) {
        
        if rightsidebar.isSidebarOpen == false {
            rightsidebar.showSidebar(true)
            rightsidebar.delegate?.rightSidebarWillOpen?()
        } else {
            
        }
        
    }
    
    
    func leftsidebarDidSelectButtonAtIndex(index: Int) {
        
        if index == 0 {
            imageView.backgroundColor = UIColor.clearColor()
            imageView.image = UIImage(named: "bg")
            leftsidebar.showSidebar(false)
            leftsidebar.delegate?.leftSidebarWillClose?()
        
        } else if index == 1 {
            imageView.backgroundColor = UIColor.grayColor()
            imageView.image = nil
            leftsidebar.showSidebar(false)
            leftsidebar.delegate?.leftSidebarWillClose?()
            
        } else if index == 2 {
            
        } else if index == 3 {
            
        } else if index == 4 {
            
        }
        
    }
    
    func rightsidebarDidSelectButtonAtIndex(index: Int) {
        
        if index == 0 {
            imageView.backgroundColor = UIColor.clearColor()
            imageView.image = UIImage(named: "bg")
            rightsidebar.showSidebar(false)
            rightsidebar.delegate?.rightSidebarWillClose?()
            
        } else if index == 1 {
            imageView.backgroundColor = UIColor.grayColor()
            imageView.image = nil
            rightsidebar.showSidebar(false)
            rightsidebar.delegate?.rightSidebarWillClose?()
            
        } else if index == 2 {
            
        } else if index == 3 {
            
        } else if index == 4 {
            
        }
        
    }

}

